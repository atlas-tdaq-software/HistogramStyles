#include "HistogramStyles/HistogramStyles.h"

#include <iostream>
#include <iomanip>

#include <TROOT.h>
#include "TFile.h"
#include "TMath.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TRandom2.h"
#include "TGraphErrors.h"

HistogramStyles* utils;

void HistoExample();
TGraphErrors* GetGraph(Int_t irr, Int_t iff,Int_t icut, Int_t ipdf);

using namespace std;

int main(int argc, char* argv[])
{
  //Intanciate the HistogramStyles class chosing environement(MON,PUB)
  utils = new HistogramStyles(HistogramStyles::PUB);
  
  //Pick the style
  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();
  
  HistoExample();
}


void HistoExample() 
{ 
  const int nren=3;

  Int_t icol1=5;
  Int_t icol2=5;

  const unsigned int NUMPDF=41;

  TCanvas* c1 = new TCanvas("c1","single inclusive jets",50,50,600,600);
  TPad* thePad = (TPad*)c1->cd();
  thePad->SetLogy();

  Double_t ymin=1.e-3;  Double_t ymax=2e7;
  Double_t xmin=60.00;  Double_t xmax=3500.;
  TH1F *h1 = thePad->DrawFrame(xmin,ymin,xmax,ymax);
  h1->SetYTitle("d#sigma_{jet}/dE_{T,jet} [fb/GeV]");
  h1->SetXTitle("E_{T,jet}  [GeV]");
  h1->GetYaxis()->SetTitleOffset(1.4);
  h1->GetXaxis()->SetTitleOffset(1.4);
  //h1->GetXaxis()->SetNdivisions(5);
  h1->Draw();

  const Int_t ncut=1;
  TGraphErrors *data[ncut];

  for (Int_t icut=0; icut<ncut; icut++) { // loop over cuts

    TGraphErrors *g1[nren][ncut];
    for (Int_t ir=0; ir<nren; ir++) { // loop over ren scale
      g1[ir][icut]= GetGraph(ir,ir,icut,0);
      if (g1[ir][icut]) 
	cout << g1[ir][icut]->GetTitle() << " found "  << g1[ir][icut]->GetName()  << endl;
      else { 
	cout << " g1 not  found " << endl; 
	return; 
      } 
      g1[ir][icut]->SetLineColor(1);
      g1[ir][icut]->SetMarkerStyle(0);
      //g1[ir][icut]->Draw("C");
    }

    char daname[100];
    sprintf(daname,"data_%d",icut); 
    data[icut]=(TGraphErrors*)g1[0][icut]->Clone(daname); 
    data[icut]->SetMarkerStyle(20);
    //data[icut]->SetMarkerColor(1);

    // just invent some data
    for (Int_t i=0; i< data[icut]->GetN(); i++) {
      TRandom2* gRandom =  new TRandom2();
      Double_t x1,y1,e,dx1=0.;
      data[icut]->GetPoint(i,x1,y1);
      Double_t r1 = 0.4*(gRandom->Rndm(1)+2);
      Double_t r2 = 0.4*(gRandom->Rndm(1)+2);
      //cout << " i= " << i << " x1= " << x1 << " y1= " << y1 << " r= " << r << endl;
      Double_t y;
      if (icut==0) y=r1*y1+r1*r2*r2*x1/50000.;
      else         y=r1*y1;
      e=sqrt(y*1000)/200;
      data[icut]->SetPoint(i, x1,y);
      data[icut]->SetPointError(i,dx1,e);
    }
    //data[icut]->Print();
  
    TGraphAsymmErrors* scale[ncut];
    TGraphAsymmErrors* scalepdf[ncut];

    scale[icut]=  utils->myMakeBand(g1[0][icut],g1[1][icut],g1[2][icut]);
    //printf(" band1: \n");
    //scale->Print();

    scalepdf[icut]=(TGraphAsymmErrors* ) scale[icut]->Clone("scalepdf");

    TGraphErrors *gpdf[NUMPDF][ncut];
    for (unsigned int ipdf=0; ipdf<NUMPDF; ipdf++) {
      gpdf[ipdf][icut]= GetGraph(0,0,icut,ipdf);
      if (gpdf[ipdf][icut]) 
	cout << gpdf[ipdf][icut]->GetTitle() << " found "  << gpdf[ipdf][icut]->GetName() << endl;
      else { 
	cout << " gpdf not  found " << endl; 
	return; 
      } 
      gpdf[ipdf][icut]->SetLineColor(2);
      gpdf[ipdf][icut]->SetLineStyle(1);
      gpdf[ipdf][icut]->SetMarkerStyle(0);
      utils->myAddtoBand(gpdf[ipdf][icut],scalepdf[icut]); 
    }

    scalepdf[icut]->SetFillColor(icol2);
    scalepdf[icut]->Draw("zE2"); 
    scale[icut]->SetFillColor(icol1);
    scale[icut]->Draw("zE2");
    g1[0][icut]->SetLineWidth(3);
    g1[0][icut]->Draw("z");
    
    data[icut]->Draw("P");
    
  }

  utils->myText(      0.4,0.88,1,"#sqrt{s}= 14 TeV");
  utils->myText(      0.7,0.88,1,"|#eta_{jet}|<0.5");
  utils->myMarkerText(0.55,0.75,1,20,"Data 2009");
  utils->myBoxText(   0.55,0.67,0.05,icol1,"NLO QCD");

  utils->AtlasLabel(0.2,0.2);
  utils->myText( 0.37,0.2,1,"Preliminary");

  c1->Print("AtlasExample.eps");
  c1->Print("AtlasExample.gif");
  c1->Print("AtlasExample.png");
  c1->Print("AtlasExample.pdf");

}

TGraphErrors* GetGraph(Int_t irr, Int_t iff,Int_t icut, Int_t ipdf) 
{ 
  TGraphErrors* g1;

  const Int_t GMAX=864;  
  const bool debug=0;

  const char *cuts[5] = 
    {"0.0 <= |eta| < 0.5",
     "0.5 <= |eta| < 1.0",
     "1.0 <= |eta| < 1.5",
     "1.5 <= |eta| < 2.0",
     "2.0 <= |eta| < 3.0"};
  
  const double mur[] = {1.0,0.25,4.0};
  const double muf[] = {1.0,0.25,4.0};
  
  TFile *f1 = new TFile("../run/nlofiles.root","READ","input");
  if (f1&&debug) cout << " file opened " << endl;

  char gname[100];
  char tname[100];

  if (debug) cout << " mur= " << mur[irr] << " muf= " << muf[iff] 
		  << " iut= " << icut << " ipdf= " << ipdf << endl;

  if (ipdf>=0)
    sprintf(tname," E_T (mu_r=%g, mu_f=%g);%s Pdf: %d",mur[irr],muf[iff],cuts[icut],ipdf);
  else
    sprintf(tname," E_T %s Ms= %d",cuts[icut],-ipdf);

  if (debug) cout << "**GetGraph: Look for: " << tname << endl;

  for (int i=1; i<=GMAX; i++){ 
    sprintf(gname,"full_%d",i);
    // sprintf(gname,"born_%d",i);
    sprintf(gname,"full_%d",i);
    g1=(TGraphErrors*) f1->Get(gname);
    if (!g1) { cout << gname << "  not found " << endl; g1=0; return 0; } 
    
    const char *title=g1->GetTitle();
    
    if (debug){ 
      cout << " Title= " << title << endl;
      cout << " tname= " << tname << endl;
      cout << " strcmp: " << strcmp(title,tname) << endl;
    }

    if (strcmp(title,tname)==0) break;
    g1=0;
  }

  if (!g1) return 0;
  if (debug) cout << " found: " << g1->GetTitle() << endl;  
  
  return g1;
} 
