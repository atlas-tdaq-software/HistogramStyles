#include "HistogramStyles/HistogramStyles.h"

#include <iostream>
#include <math.h>


#include "TH1.h" 
#include "TPave.h" 
#include "TLine.h" 
#include "TMarker.h" 
#include "TLatex.h"


using namespace std;

HistogramStyles *HistogramStyles::rootUtils = 0;

//__________________________________________________________________________________________________
HistogramStyles::HistogramStyles(styleType type)
{ 
  createHistogramStyles(type);
}

//__________________________________________________________________________________________________
void HistogramStyles::createHistogramStyles(styleType type){

  atlasStyle= new TStyle("ATLAS","Atlas style");

  std::cout << "Setting ATLAS Style for ";
  if(type==MON) std::cout << "Monitoring." << std::endl;
  if(type==PUB) std::cout << "Publication." << std::endl;

  // Workaround for the ROOT bug https://savannah.cern.ch/bugs/?93785
  atlasStyle->SetFrameFillColor(10);

  // Canvas settings
  Int_t icol=0;
  atlasStyle->SetFrameBorderMode(icol);
  atlasStyle->SetCanvasBorderMode(icol);
  atlasStyle->SetPadBorderMode(icol);
  atlasStyle->SetPadColor(icol);
  atlasStyle->SetCanvasColor(icol);
  atlasStyle->SetStatColor(icol);
  
  // set the paper & margin sizes
  atlasStyle->SetPaperSize(20,26);
  if(type==MON){
    atlasStyle->SetPadTopMargin(0.20);
    atlasStyle->SetPadRightMargin(0.12);
    atlasStyle->SetPadBottomMargin(0.10);
    atlasStyle->SetPadLeftMargin(0.10);
  }
  if(type==PUB){
    atlasStyle->SetPadTopMargin(0.05);
    atlasStyle->SetPadRightMargin(0.05);
    atlasStyle->SetPadBottomMargin(0.16);
    atlasStyle->SetPadLeftMargin(0.16);
  }

  // use large fonts
  Int_t font=42;//Helvetica
  atlasStyle->SetTextFont(font);
  atlasStyle->SetTextSize(0.05);
  atlasStyle->SetTitleFont(font,"xyz");
  atlasStyle->SetTitleOffset(1.,"xyz");
  atlasStyle->SetTitleSize(0.05,"xyz");
  atlasStyle->SetTickLength(0.02,"xyz");
  atlasStyle->SetLabelFont(font,"xyz");
  atlasStyle->SetLabelOffset(0.01, "xyz");
  atlasStyle->SetLabelSize(0.04,"xyz");
  atlasStyle->SetAxisColor(kGray, "xyz");

  //palette settings
  atlasStyle->SetPalette(1,0);

  // Qt4 is very slow with drawing Ellipses
  // Let's reduce the markers size to make them drawn as dots
  atlasStyle->SetMarkerSize(0.01);
  atlasStyle->SetMarkerStyle(20);
  atlasStyle->SetHistLineWidth(2);
  atlasStyle->SetLineStyleString(12,"4 12"); // style for reference histograms
  
  atlasStyle->SetStatStyle(0);
  atlasStyle->SetStatFont(font);
  atlasStyle->SetStatFontSize(0.035); //0.25
  atlasStyle->SetStatBorderSize(0);
  if(type==MON){
    atlasStyle->SetOptStat("ourme");
    atlasStyle->SetOptTitle(1);
    atlasStyle->SetStatH(0.2);
    atlasStyle->SetStatW(0.3);
  }
  if(type==PUB){
    atlasStyle->SetStatX(0.99); //0.6
    atlasStyle->SetStatY(0.99);
    atlasStyle->SetStatH(0.01); //0.01
    atlasStyle->SetStatW(0.2);
    atlasStyle->SetOptStat(0);
    atlasStyle->SetOptTitle(0);
  }
  atlasStyle->SetOptFit(0);

  atlasStyle->SetTitleFontSize(0.05);
  atlasStyle->SetTitleBorderSize(0);
  atlasStyle->SetTitleStyle(0);
  atlasStyle->SetTitleH(0.05);

  // put tick marks on top and RHS of plots
  atlasStyle->SetPadTickX(1);
  atlasStyle->SetPadTickY(1);

  // Lines with width=1 are not drawn with ROOT 6 and Qt
  atlasStyle->SetLineWidth(2);
}

//__________________________________________________________________________________________________
void HistogramStyles::AtlasLabel(Double_t x,Double_t y,Color_t color)
{
  Int_t font=72;

  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(font);
  l.SetTextColor(color);
  l.DrawLatex(x,y,"ATLAS");
}

//__________________________________________________________________________________________________
TGraphErrors* HistogramStyles::myTGraphErrorsDivide(TGraphErrors* g1,TGraphErrors* g2)
{
  TGraphErrors* g3= new TGraphErrors();

  if (!g1) {
    printf("**myTGraphErrorsDivide: g1 does not exist !  \n"); 
    return g3;
  }
  if (!g2) {
    printf("**myTGraphErrorsDivide: g2 does not exist !  \n"); 
    return g3;
  }

  if (g1->GetN()!=g2->GetN()) {
   printf("**myTGraphErrorsDivide: vector do not have same number of entries !  \n"); 
  }

  Int_t iv=0;
  for (Int_t i1=0; i1<g1->GetN(); i1++) {
   for (Int_t i2=0; i2<g2->GetN(); i2++) {
     Double_t  x1=0., y1=0., x2=0., y2=0.;
     Double_t dx1=0.,dy1=0.,       dy2=0.;
     Double_t e=0.;
     
     g1->GetPoint(i1,x1,y1);
     g2->GetPoint(i2,x2,y2);
     if(x1==x2){
       dx1  = g1->GetErrorX(i1);
       if (y1!=0) dy1  = g1->GetErrorY(i1)/y1;
       if (y2!=0) dy2  = g2->GetErrorY(i2)/y2;
       
       if (y2!=0.) g3->SetPoint(iv, x1,y1/y2);
       else        g3->SetPoint(iv, x1,y2);
       
       if (y1!=0 && y2!=0) e=sqrt(dy1*dy1+dy2*dy2)*(y1/y2); 
       g3->SetPointError(iv,dx1,e);

       iv++;
     }
     else{
       printf("**myTGraphErrorsDivide: %d x1!=x2  %f %f  !  \n",iv,x1,x2);
     }
   }
  }
  return g3;
}


//__________________________________________________________________________________________________
TGraphAsymmErrors* HistogramStyles::myTGraphErrorsDivide(TGraphAsymmErrors* g1,TGraphAsymmErrors* g2)
{
 const Int_t debug=0; 

  TGraphAsymmErrors* g3= new TGraphAsymmErrors();
  Int_t n1=g1->GetN();
  Int_t n2=g2->GetN();

  if (n1!=n2) {
    printf(" vectors do not have same number of entries !  \n");
   return g3;
  }

  Double_t   x1=0.,   y1=0., x2=0., y2=0.;
  Double_t dx1h=0., dx1l=0.;
  Double_t dy1h=0., dy1l=0.;
  Double_t dy2h=0., dy2l=0.;

  //Double_t* X1 = g1->GetX();
  //Double_t* Y1 = g1->GetY();
  Double_t* EXhigh1 = g1->GetEXhigh();
  Double_t* EXlow1 =  g1->GetEXlow();
  Double_t* EYhigh1 = g1->GetEYhigh();
  Double_t* EYlow1 =  g1->GetEYlow();

  //Double_t* X2 = g2->GetX();
  //Double_t* Y2 = g2->GetY();
  //Double_t* EXhigh2 = g2->GetEXhigh();
  //Double_t* EXlow2 =  g2->GetEXlow();
  Double_t* EYhigh2 = g2->GetEYhigh();
  Double_t* EYlow2 =  g2->GetEYlow();

  for (Int_t i=0; i<g1->GetN(); i++) {
    g1->GetPoint(i,x1,y1);
    g2->GetPoint(i,x2,y2);
    dx1h  = EXhigh1[i];
    dx1l  = EXlow1[i];
    if (y1!=0.) dy1h  = EYhigh1[i]/y1;
    else        dy1h  = 0.;
    if (y2!=0.) dy2h  = EYhigh2[i]/y2;
    else        dy2h  = 0.;
    if (y1!=0.) dy1l  = EYlow1 [i]/y1;
    else        dy1l  = 0.;
    if (y2!=0.) dy2l  = EYlow2 [i]/y2;
    else        dy2l  = 0.;
   
    if (debug)
      printf("%d dy1=%f %f dy2=%f %f sqrt= %f %f \n",i,dy1l,dy1h,dy2l,dy2h,
              sqrt(dy1l*dy1l+dy2l*dy2l),sqrt(dy1h*dy1h+dy2h*dy2h));

    if (y2!=0.) g3->SetPoint(i, x1,y1/y2);
    else       g3->SetPoint(i, x1,y2);
    Double_t el=0.; Double_t eh=0.;
    
    if (y1!=0. && y2!=0.) el=sqrt(dy1l*dy1l+dy2l*dy2l)*(y1/y2);
    if (y1!=0. && y2!=0.) eh=sqrt(dy1h*dy1h+dy2h*dy2h)*(y1/y2);
    
    if (debug) printf("dx1h=%f  dx1l=%f  el=%f  eh=%f \n",dx1h,dx1l,el,eh);
    g3->SetPointError(i,dx1h,dx1l,el,eh);
    
  }  
  return g3;
}


//__________________________________________________________________________________________________
TGraphAsymmErrors*  HistogramStyles::myMakeBand(TGraphErrors* g0, TGraphErrors* g1,TGraphErrors* g2)
{
  TGraphAsymmErrors* g3= new TGraphAsymmErrors();
  
  Double_t  x1=0., y1=0., x2=0., y2=0., y0=0, x3=0.;
  Double_t dum;
  for (Int_t i=0; i<g1->GetN(); i++) {
    g0->GetPoint(i, x1,y0);
    g1->GetPoint(i, x1,y1);
    g2->GetPoint(i, x1,y2);

    if (i==g1->GetN()-1) x2=x1;
    else                 g2->GetPoint(i+1,x2,dum);

    if (i==0)            x3=x1;
    else                 g2->GetPoint(i-1,x3,dum);
    
    Double_t tmp=y2;
    if (y1<y2) {y2=y1; y1=tmp;}
    Double_t y3=y0;
    g3->SetPoint(i,x1,y3);

    Double_t binwl=(x1-x3)/2.;
    Double_t binwh=(x2-x1)/2.;
    if (binwl==0.)  binwl= binwh;
    if (binwh==0.)  binwh= binwl;
    g3->SetPointError(i,binwl,binwh,(y3-y2),(y1-y3));

  }
  return g3;

}

//__________________________________________________________________________________________________
void HistogramStyles::myAddtoBand(TGraphErrors* g1, TGraphAsymmErrors* g2)
{
  Double_t  x1=0., y1=0.,  y2=0., y0=0;

  if (g1->GetN()!=g2->GetN())
   cout << " graphs have not the same # of elements " << endl;

  Double_t* EYhigh = g2-> GetEYhigh();
  Double_t* EYlow  = g2-> GetEYlow();

  for (Int_t i=0; i<g1->GetN(); i++) {
    g1->GetPoint(i, x1,y1);
    g2->GetPoint(i, x1,y2);

    if (y1==0) y1=1;
    if (y2==0) y2=1;

    Double_t eyh=0., eyl=0.;

    y0=y1-y2;
    if (y0!=0) {
      if (y0>0) {
	eyh=EYhigh[i];
	eyh=sqrt(eyh*eyh+y0*y0);
	g2->SetPointEYhigh(i,eyh);
      } else {
	eyl=EYlow[i];
	eyl=sqrt(eyl*eyl+y0*y0);
	g2->SetPointEYlow (i,eyl);
      }
    }
  }
  return;

}


//__________________________________________________________________________________________________
TGraphErrors* HistogramStyles::TH1ToTGraph(TH1 *h1)
{
  if (!h1) cout << "TH1TOTGraph: histogram not found !" << endl;
  
  TGraphErrors* g1= new TGraphErrors();
  
  Double_t x, y, ex, ey;
  for (Int_t i=0; i<h1->GetNbinsX(); i++) {
    y=h1->GetBinContent(i);
    ey=h1->GetBinError(i);
    x=h1->GetBinCenter(i);
    ex=h1->GetBinWidth(i);

    g1->SetPoint(i,x,y);
    g1->SetPointError(i,ex,ey);
  }
  
 return g1;
}


//__________________________________________________________________________________________________
void HistogramStyles::myText(Double_t x,Double_t y,Color_t color,const char *text)
{
  TLatex l; 
  //l.SetTextAlign(12); l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);

}


//__________________________________________________________________________________________________
void HistogramStyles::myBoxText(Double_t x, Double_t y,Double_t boxsize,Int_t mcolor,const char *text)
{
  Double_t tsize=0.06;

  TLatex l; l.SetTextAlign(12); //l.SetTextSize(tsize); 
  l.SetNDC();
  l.DrawLatex(x,y,text);

  Double_t y1=y-0.25*tsize;
  Double_t y2=y+0.25*tsize;
  Double_t x2=x-0.3*tsize;
  Double_t x1=x2-boxsize;

  printf("x1= %f x2= %f y1= %f y2= %f \n",x1,x2,y1,y2);

  TPave *mbox= new TPave(x1,y1,x2,y2,0,"NDC");

  mbox->SetFillColor(mcolor);
  mbox->SetFillStyle(1001);
  mbox->Draw();

  TLine mline;
  mline.SetLineWidth(4);
  mline.SetLineColor(1);
  mline.SetLineStyle(1);
  Double_t y3=(y1+y2)/2.;
  mline.DrawLineNDC(x1,y3,x2,y3);
}


//__________________________________________________________________________________________________
void HistogramStyles::myMarkerText(Double_t x,Double_t y,Int_t color,Int_t mstyle,const char *text)
{
  Double_t tsize=0.06;
  TMarker *marker = new TMarker(x-(0.4*tsize),y,8);
  marker->SetMarkerColor(color);  marker->SetNDC();
  marker->SetMarkerStyle(mstyle);
  marker->SetMarkerSize(2.0);
  marker->Draw();

  TLatex l; l.SetTextAlign(12); //l.SetTextSize(tsize); 
  l.SetNDC();
  l.DrawLatex(x,y,text);
}

//_____________________________________________________________________________
void HistogramStyles::myLegendSetting(TLegend* legd){
  legd->SetFillColor(10);
  legd->SetFillStyle(0);
  legd->SetBorderSize(0);
  legd->SetTextFont(42);
  legd->SetTextSize(0.04);//0.04
}
