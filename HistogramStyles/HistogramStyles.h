#ifndef HISTOGRAMSTYLES_H
#define HISTOGRAMSTYLES_H

/*! \file HistogramStyle declares the ATLAS Histogram ROOT settings.
 * \author Anyes Taffard, Marco Delmastro
 * \version 1.0
 */

#include "TStyle.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TLegend.h"


/*! \class HistogramStyles
 *  This class contains the functionalities to format the ROOT canvas and plots according to the ATLAS standard
 */
class HistogramStyles
{
 public: 
  enum styleType {MON,PUB};
  static HistogramStyles *rootUtils;
  TStyle *atlasStyle;

  /** Constructor 
     @param type Set the style for monitoring(MON) or publication(PUB)
   */ 
  HistogramStyles(styleType type=MON);

  virtual ~HistogramStyles(){};
 
  /** Draw ALTAS label  */
  void AtlasLabel(Double_t x,Double_t y,Color_t color=1);
  
  /** Divide two TGraphErrors 
     @return Pointer to new TGraphErrors
   */
  TGraphErrors* myTGraphErrorsDivide(TGraphErrors* g1,TGraphErrors* g2);
  /** Divide two TGraphAsymmErrors 
     @return Pointer to new TGraphAsymmErrors
   */
  TGraphAsymmErrors* myTGraphErrorsDivide(TGraphAsymmErrors* g1,TGraphAsymmErrors* g2);
  /** 
     Compute error band given nominal, upper and lower values
     @return Pointer to new TGraphAsymmErrors
   */
  TGraphAsymmErrors* myMakeBand(TGraphErrors* g0, TGraphErrors* g1,TGraphErrors* g2);
  /**
    Convert TH1 into TGraphErrors
    @return Pointer to new TGraphErrors
   */
  TGraphErrors* TH1ToTGraph(TH1 *h1);
  /**
    Add error to TGraphAsymmErrors
   */
  void myAddtoBand(TGraphErrors* g1, TGraphAsymmErrors* g2);
  
  /** 
     Draw text at coordinates
  */
  void myText(Double_t x,Double_t y,Color_t color,const char *text);
  /**
     Draw colored box with line followed by text
  */
  void myBoxText(Double_t x, Double_t y,Double_t boxsize,Int_t mcolor,const char *text);
  /**
     Draw dot followed by text
   */
  void myMarkerText(Double_t x,Double_t y,Int_t color,Int_t mstyle,const char *text);
  /**
    Define legend styles
  */
  void myLegendSetting(TLegend* legd);
  
  
 private:
  /**
     Create atlasStyle
   */
  void createHistogramStyles(styleType type);
  
  
};

#endif // HISTOGRAMSTYLE_H
